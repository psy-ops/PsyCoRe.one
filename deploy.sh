### In Plesk, execute with:
### bash deploy.sh >> deployment.log 2>&1
### Cran job for executing every hour with logging
### @hourly bash ~/git_clone_hpss/deploy.sh >> ~/git_clone_hpss/deployment.log 2>&1
### https://www.cyberciti.biz/faq/how-do-i-add-jobs-to-cron-under-linux-or-unix-oses/
### To edit the cron file with nano instead of vim:
### export VISUAL=nano; crontab -e

echo ----------
echo $(date)

### Go to directory with cloned git repo
cd ~/deploy_psycore.one

echo Running Quarto...

### Render the site
/usr/local/bin/quarto render

echo Done with Quarto. Deleting old directories and files...

### Delete all contents in public HTML directory
rm -rf ~/public_html/*.*
rm -rf ~/public_html/*
rm -f ~/public_html/.htaccess

echo Done deleting old directories and files. Copying over new website...

### Copy website
cp -RT public ~/public_html

### Copy .htaccess
cp .htaccess ~/public_html

echo Done copying over new website.

echo ----------
